*** Setting ***
Library     String
Library     SeleniumLibrary

*** Variables ***
${browser}      chrome
${homepage}     automationpractice.com/index.php
${scheme}   http
${testUrl}      ${scheme}://${homepage}

*** Keywords ***
Open Homepage
    Open browser    ${testUrl}      ${browser}

*** Test Cases ***
C001 Hacer Clic en Contenedores
    Open Homepage
    Maximize Browser Window
    Set Global Variable     @{nombresDeContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a      //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a      //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    FOR    ${nombreDeContenedores}    IN   @{nombresDeContenedores}
        Wait Until Element Is Visible    xpath=${nombreDeContenedores}
        Click Element     xpath=${nombreDeContenedores}
        Wait Until Element Is Visible   xpath= //*[@id="bigpic"]
        Capture Page Screenshot     Screenshot.png
        Click Element   xpath= //*[@id="header_logo"]/a/img
    END
    Close Browser
C002 Caso de Prueba Nuevo
    Open Homepage